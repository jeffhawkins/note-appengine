(function () {
  'using strict';

  angular.module('notesApp').factory('noteService', [
    function() {
      var selectedNoteId;

      var setSelectedNoteId = function(id) {
        selectedNoteId = id;
      };

      var getSelectedNoteId = function() {
        return selectedNoteId;
      };

      return {
        setSelectedNoteId:setSelectedNoteId,
        getSelectedNoteId:getSelectedNoteId
      };
    }
  ]);
})();
