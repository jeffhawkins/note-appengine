(function () {
  'using strict';

  angular.module('notesApp').controller('ShellController', [
    '$scope',
    '$log',
    '$http',
    '$location',
    function ($scope, $log, $http, $location) {
      $scope.showSpinner = false;

      $scope.init = function() {
        $http.get('/notes/v1/note/user/name').
          then(function(response) {
            $scope.shell = {
              email: response.data.name,
              logout: response.data.logout
            };
          });
      };

      $scope.profile = function() {
        $log.log('You clicked on the profile.');
      };

      $scope.$on('show-spinner', function() {
        $scope.showSpinner = true;
      });

      $scope.$on('hide-spinner', function() {
        $scope.showSpinner = false;
      });

    }
  ]);
})();
