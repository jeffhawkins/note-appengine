(function () {
  'using strict';

  angular.module('notesApp').controller('AddNoteController', [
    '$scope',
    '$log',
    '$location',
    '$http',
    'spinService',
    function ($scope, $log, $location, $http, spinService) {
      $scope.note = {
        id: '',
        title: '',
        note: ''
      };

      $scope.cancel = function() {
        $location.path('/notes');
      };

      $scope.save = function() {
        spinService.start();

        if ($scope.note.id !== '') {
          $http.put('/notes/v1/note', $scope.note).
            then(function(response) {
              spinService.stop();
            });
        } else {
          $http.post('/notes/v1/note', $scope.note).
            then(function(response) {
              $scope.note.id = response.data.id;
              spinService.stop();
            });
        }
      };
    }
  ]);
})();