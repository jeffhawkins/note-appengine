(function () {
  'using strict';

  angular.module('notesApp').directive('loadingIndicator', [
      'loadingIndicatorService',
      function(loadingIndicatorService) {
      return {
          restrict: 'C',
          link: function(scope, elm, attrs, ctrl) {
              if (loadingIndicatorService.isShowing() === true) {
                  elm.addClass('hidden');
              } else {
                  elm.removeClass('hidden');
              }
          }
      };
  }]);
})();