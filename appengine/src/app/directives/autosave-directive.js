(function () {
  'using strict';

  angular.module('notesApp').directive('autosave', [
    '$rootScope',
    '$log',
    '$interval',
    function($rootScope, $log, $interval) {
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          var started = false;
          var intervalPromise = null;

          element.on('keyup', function(event) {
            $log.log('keyup...');
            
            if (!started) {
              started = true;
              intervalPromise = $interval(function(){
                $rootScope.$broadcast('note-save');
              }, 2000);
            }

          });

          scope.$on('$destroy', function () {
            console.log('captured $destroy event');
            $interval.cancel(intervalPromise);
          });

        }
      };
    }
  ]);
})();