(function () {
  'using strict';

  angular.module('notesApp').filter('moment', function() {
    return function(input) {
      // Mar 28, 2016 9:58:32 PM
      // "2016-03-29T01:58:32.212428Z"
      var m = moment(input);
      return m.calendar();
    };
  });
})();
