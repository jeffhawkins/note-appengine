package notes

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"google.golang.org/appengine"
	"google.golang.org/appengine/blobstore"
	"google.golang.org/appengine/datastore"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/user"
)

type Attachment struct {
	BlobKey  string    `json:"blobKey"`
	Created  time.Time `json:"created"`
	Filename string    `json:"filename"`
	Size     int64     `json:"size"`
}

type Note struct {
	Id        string    `json:"id" datastore:"-"`
	Created   time.Time `json:"created"`
	Updated   time.Time `json:"updated"`
	Title     string    `json:"title"`
	Note      string    `json:"note" datastore:",noindex"` // Allows storage of 1M in bytes. Without this only 1500 bytes.
	UserId    string    `json:"-"`
	UserEmail string    `json:"-"`
}

func SetRoutes(r *mux.Router) {
	r.HandleFunc("/notes/v1/note", postHandler).Methods("POST")
	r.HandleFunc("/notes/v1/note", putHandler).Methods("PUT")
	r.HandleFunc("/notes/v1/note", getAllHandler).Methods("GET")
	r.HandleFunc("/notes/v1/note/{id}", getHandler).Methods("GET")
	r.HandleFunc("/notes/v1/note/user/name", getUserNameHandler).Methods("GET")
	r.HandleFunc("/notes/v1/note/{id}", deleteHandler).Methods("DELETE")

	r.HandleFunc("/notes/v1/upload/url", getUploadUrlHandler).Methods("GET")
	r.HandleFunc("/notes/v1/upload", postUploadHandler).Methods("POST")
	r.HandleFunc("/notes/v1/attachments", getAllAttachmentsHandler).Methods("GET")
	r.HandleFunc("/notes/v1/download/{id}", getDownloadHandler).Methods("GET")
	r.HandleFunc("/notes/v1/download/{id}", deleteAttachementHandler).Methods("DELETE")

	r.HandleFunc("/notes/v1/archive", getAllArchivesHandler).Methods("GET")
	r.HandleFunc("/notes/v1/archive/{id}", getArchiveHandler).Methods("GET")
	r.HandleFunc("/notes/v1/archive/{id}", moveArchiveHandler).Methods("PUT")
	r.HandleFunc("/notes/v1/archive/{id}", deleteArchiveHandler).Methods("DELETE")
}

//
// Add a new note
//
func postHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := user.Current(c)
	log.Infof(c, "Handling post of new note...")

	b, e := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to read request body.", http.StatusInternalServerError)
		return
	}

	var note Note
	e = json.Unmarshal(b, &note)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to unmarshal request body.", http.StatusInternalServerError)
		return
	}

	note.Created = time.Now()
	note.Updated = time.Now()
	note.UserId = u.ID
	note.UserEmail = u.Email

	key := datastore.NewIncompleteKey(c, "Note", nil)
	key, e = datastore.Put(c, key, &note)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to store note.", http.StatusInternalServerError)
		return
	}

	// Try and get the note we just added. This seems to fix an issue in the development
	// server at least where the most recently added note does not come back in the
	// getAll... request.
	if appengine.IsDevAppServer() == true {
		e = datastore.Get(c, key, &note)
		if e != nil {
			log.Errorf(c, "Error: %v", e)
			http.Error(w, "Failed to store note.", http.StatusInternalServerError)
			return
		}
	}

	// Return the key of the Note that was just stored.
	s := fmt.Sprintf("{\"id\":\"%v\"}", key.Encode())
	w.Header().Set("content-type", "application/json")
	w.Write([]byte(s))
}

//
// Update a note
//
func putHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	u := user.Current(c)

	log.Infof(c, "Handling put of new note...")

	b, e := ioutil.ReadAll(r.Body)
	defer r.Body.Close()

	log.Infof(c, "body: %v", string(b))

	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to read request body.", http.StatusInternalServerError)
		return
	}

	note := new(Note)
	e = json.Unmarshal(b, &note)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to unmarshal request body.", http.StatusInternalServerError)
		return
	}

	k, e := datastore.DecodeKey(note.Id)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to decode key.", http.StatusInternalServerError)
		return
	}

	note.Updated = time.Now()
	note.UserId = u.ID
	note.UserEmail = u.Email

	_, e = datastore.Put(c, k, note)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to store note.", http.StatusInternalServerError)
		return
	}
}

//
// Get all notes
//
func getAllHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Infof(c, "Getting all notes...")

	u := user.Current(c)
	log.Infof(c, "user id: %v", u.ID)
	log.Infof(c, "user email: %v", u.Email)

	q := datastore.NewQuery("Note").Filter("UserId =", u.ID).Order("-Updated")
	var notes []Note
	keys, err := q.GetAll(c, &notes)
	if err != nil {
		log.Errorf(c, "fetching notes: %v", err)
		http.Error(w, "Failed gettng notes from datastore.", http.StatusInternalServerError)
		return
	}

	for i, key := range keys {
		notes[i].Id = key.Encode()
	}

	b, e := json.Marshal(notes)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to marshal datastore results.", http.StatusInternalServerError)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.Write(b)
}

//
// Get a note by id
//
func getHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Infof(c, "Getting a note...")

	vars := mux.Vars(r)
	id := vars["id"]
	if id == "" {
		http.Error(w, "No id specified in request", http.StatusBadRequest)
		return
	}

	k, e := datastore.DecodeKey(id)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to marshal datastore results.", http.StatusInternalServerError)
		return
	}

	var note Note
	err := datastore.Get(c, k, &note)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	note.Id = k.Encode()

	b, e := json.Marshal(&note)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to marshal datastore results.", http.StatusInternalServerError)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.Write(b)
}

func deleteHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Infof(c, "Deleting note...")

	vars := mux.Vars(r)
	id := vars["id"]
	if id == "" {
		http.Error(w, "No id specified in request", http.StatusBadRequest)
		return
	}

	k, e := datastore.DecodeKey(id)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to marshal datastore results.", http.StatusInternalServerError)
		return
	}

	note := new(Note)
	err := datastore.Get(c, k, note)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	key := datastore.NewIncompleteKey(c, "Archive", nil)
	key, e = datastore.Put(c, key, note)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to store note.", http.StatusInternalServerError)
		return
	}

	e = datastore.Delete(c, k)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed delete note from datastore.", http.StatusInternalServerError)
		return
	}
}

func getUserNameHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Infof(c, "Getting user name...")
	u := user.Current(c)

	l, e := user.LogoutURL(c, "/")
	if e != nil {
		log.Errorf(c, "Error: %v", e)
	}

	s := `{
			"name" : "%s",
			"logout" : "%s"
		  }`

	s = fmt.Sprintf(s, u.String(), l)
	log.Infof(c, "user info: %s", s)

	w.Header().Set("content-type", "application/json")
	w.Write([]byte(s))
}

func getUploadUrlHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Infof(c, "getUploadHandler")

	u := user.Current(c)

	var bucket = "all-notes.appspot.com/" + u.ID

	options := &blobstore.UploadURLOptions{
		StorageBucket: bucket,
	}

	url, e := blobstore.UploadURL(c, "/notes/v1/upload", options)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed get upload url.", http.StatusInternalServerError)
		return
	}

	s := `{"url" : "%s"}`
	s = fmt.Sprintf(s, url)
	log.Infof(c, "url: %s", s)

	w.Header().Set("content-type", "application/json")
	w.Write([]byte(s))
}

func postUploadHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	blobs, _, err := blobstore.ParseUpload(r)
	if err != nil {
		log.Errorf(c, "Error: %v", err)
		http.Error(w, "Failed to upload.", http.StatusInternalServerError)
		return
	}

	files := blobs["file"]
	if len(files) == 0 {
		log.Errorf(c, "no file uploaded")
		http.Error(w, "No files found to upload.", http.StatusInternalServerError)
		return
	}

	for file := range files {
		key := datastore.NewIncompleteKey(c, "Attachment", nil)

		a := Attachment{
			BlobKey:  string(files[file].BlobKey),
			Created:  files[file].CreationTime,
			Filename: files[file].Filename,
			Size:     files[file].Size,
		}

		_, e := datastore.Put(c, key, &a)
		if e != nil {
			log.Errorf(c, "Error: %v", e)
			http.Error(w, "Failed to store note.", http.StatusInternalServerError)
			return
		}
	}

	url := fmt.Sprintf("/#/attachments")

	http.Redirect(w, r, url, http.StatusFound)
}

func getDownloadHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	vars := mux.Vars(r)
	id := vars["id"]
	if id == "" {
		http.Error(w, "No id specified in request", http.StatusBadRequest)
		return
	}

	log.Infof(c, "id = %v", id)

	info, e := blobstore.Stat(c, appengine.BlobKey(id))
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed on Stat.", http.StatusInternalServerError)
		return
	}

	s := fmt.Sprintf("attachment; filename=\"%v\"", info.Filename)
	w.Header().Set("Content-Disposition", s)
	blobstore.Send(w, info.BlobKey)
}

func deleteAttachementHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)

	log.Infof(c, "Deleting attachment...")

	vars := mux.Vars(r)
	id := vars["id"]
	if id == "" {
		http.Error(w, "No blobKey specified in request", http.StatusBadRequest)
		return
	}

	log.Infof(c, "We have an id... %v", id)

	q := datastore.NewQuery("Attachment").Filter("BlobKey =", id)
	var attachments []Attachment
	keys, e := q.GetAll(c, &attachments)
	if e != nil {
		log.Errorf(c, "fetching attachments: %v", e)
		http.Error(w, "Failed gettng attachments from datastore.", http.StatusInternalServerError)
		return
	}

	log.Infof(c, "Found Attachment")

	// Delete the file in the Cloud Storage
	e = blobstore.Delete(c, appengine.BlobKey(id))
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to delete blob.", http.StatusInternalServerError)
		return
	}

	// Delete the Attachment record
	e = datastore.Delete(c, keys[0])
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to delete attachment.", http.StatusInternalServerError)
		return
	}
}

func getAllAttachmentsHandler(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	log.Infof(c, "Getting all attachments...")

	q := datastore.NewQuery("Attachment").Order("-Created")
	var attachments []Attachment
	_, err := q.GetAll(c, &attachments)
	if err != nil {
		log.Errorf(c, "fetching attachments: %v", err)
		http.Error(w, "Failed gettng attachments from datastore.", http.StatusInternalServerError)
		return
	}

	b, e := json.Marshal(attachments)
	if e != nil {
		log.Errorf(c, "Error: %v", e)
		http.Error(w, "Failed to marshal datastore results.", http.StatusInternalServerError)
		return
	}

	w.Header().Set("content-type", "application/json")
	w.Write(b)
}
