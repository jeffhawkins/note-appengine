package utils

import (
	"github.com/jameskeane/bcrypt"
)

func EncryptPassword(password string) (string, error) {
	crypt, err := bcrypt.Hash(password)
	if err != nil {
		return "", err
	}

	return crypt, nil
}

func VerifyPassword(hash string, password string) bool {
	if bcrypt.Match(password, hash) {
		return true
	} else {
		return false
	}
}
